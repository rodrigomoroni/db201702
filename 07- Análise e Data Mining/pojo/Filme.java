/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author rodrigo
 */
public class Filme {
private String identificador;
private String nome;


private int nota;

    public Filme(String identificador, int nota, String nome) {
        this.identificador = identificador;
        this.nota = nota;
        this.nome = nome;
    }

    public int getNota() {
        return nota;
    }

    public String getIdentificador() {
        return identificador;
    }
        public String getNome() {
        return nome;
    }
}
