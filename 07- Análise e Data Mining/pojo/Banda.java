/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author rodrigo
 */
public class Banda {
private String identificador;
private int nota;
private String nome;

    public Banda(String identificador, int nota, String nome) {
        this.identificador = identificador;
        this.nota = nota;
        this.nome = nome;
    }


    public String getNome() {
        return nome;
    }

    public int getNota() {
        return nota;
    }

    public String getIdentificador() {
        return identificador;
    }
}
