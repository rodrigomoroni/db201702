/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd07;

import dao.BandaDao;
import dao.FilmeDao;
import dao.PessoaDao;
import java.util.List;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import pojo.Banda;
import pojo.Filme;
import pojo.Pessoa;

/**
 *
 * @author rodrigo
 */
public class main{

    public static void main(String[] args) {
        
        BandaDao banda = new BandaDao();
        FilmeDao filme = new FilmeDao();
        PessoaDao pessoa = new PessoaDao();
        
        /*Média e Desvio Padrão do Rating das Bandas*/
        System.out.println("Média e Desvio Padrão:");
        System.out.println("BANDAS:");
        System.out.println("\tMédia: " + banda.getMedia() + " - Desvio Padrão: " + banda.getDesvioPadrao());
        
        /*Média e Desvio Padrão do Rating dos Artistas*/       
        System.out.println("FILMES:");
        System.out.println("\tMédia:" + filme.getMedia() + " - Desvio Padrão: " + filme.getDesvioPadrao());
        
        System.out.println("\nMaiores Rating:");
        /*Maiores Rating Médio Curtidos por >= 2 pessoas - Bandas*/
        List<Banda> bandas;
        bandas = banda.getMaioresRatingMedio();
        System.out.println("BANDAS:");
        bandas.forEach((band) ->{
            System.out.println("\t" + band.getNome());
        });
        
        /*Maiores Rating Médio Curtidos por >= 2 pessoas - Filmes*/
        List<Filme> filmes;
        filmes = filme.getMaioresRatingMedio();
        System.out.println("FILMES:");
        filmes.forEach((film) -> {
            System.out.println("\t" + film.getNome());
        });
        
        System.out.println("\nMais Populares:");
        /*10 Mais Populares - Bandas*/
        bandas.clear();
        bandas = banda.getMaisPop();
        System.out.println("BANDAS:");
        bandas.forEach((band) ->{
            System.out.println("\t" + band.getNome());
        });
        
        /*10 Mais Populares - Filmes*/
        filmes.clear();
        filmes = filme.getMaisPop();
        System.out.println("FILMES:");
        filmes.forEach((film) -> {
            System.out.println("\t" + film.getNome());
        });
        
        /*Mais Filmes em Comum Entre Conhecidos)*/
        System.out.println("\nMais Filmes Em Comum Entre Conhecidos:");
        List<Pessoa> pessoas;
        pessoas = pessoa.GetAllFilmesConhecidos();
        pessoas.forEach((person) -> {
            System.out.println("\t" + person.getNome() + " - " + person.getConhecido().getNome());
        });
        
        /*Conhecidos de Conhecidos para Cada Integrante*/
        System.out.println("\nNúmero de Conhecidos de Conhecidos:");
        System.out.println("Buranello:");
        System.out.println("\t" + pessoa.GetQtdConhecidos("http://utfpr.edu.br/CSB30/2017/2/buranelloambrosio"));
        
        
        /*************Gráficos*****************************/
        /*Gráfico f(x) pessoas x bandas*/
        /*DefaultCategoryDataset dsBanda;
        dsBanda = banda.getPessoaBanda();
        JFreeChart graficoBanda = ChartFactory.createBarChart("Pessoas X Bandas", "Bandas", 
    "Pessoas", dsBanda, PlotOrientation.VERTICAL, true, true, false);
        JFrame bandaFrame = new JFrame("Banda");
        bandaFrame.add(new ChartPanel(graficoBanda));
        bandaFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        bandaFrame.pack();
        bandaFrame.setVisible(true);*/
        
        /*Gráfico f(x) pessoas x filmes*/
        DefaultCategoryDataset dsFilme;
        dsFilme = filme.getPessoaFilme();
        JFreeChart graficoFilme = ChartFactory.createBarChart("Pessoas X Filmes", "Filmes", 
    "Pessoas", dsFilme, PlotOrientation.VERTICAL, true, true, false);
        JFrame filmeFrame = new JFrame("Filme");
        filmeFrame.add(new ChartPanel(graficoFilme));
        filmeFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        filmeFrame.pack();
        filmeFrame.setVisible(true);
        
        /*Gráfico f(x) filmes x pessoas*/
        DefaultCategoryDataset dsFilmePessoa;
        dsFilmePessoa = filme.getFilmePessoa();
        JFreeChart graficoFilmePessoa = ChartFactory.createBarChart("Filmes X Pessoas", "Pessoas", 
    "Filmes", dsFilmePessoa, PlotOrientation.VERTICAL, true, true, false);
        JFrame filmePessoaFrame = new JFrame("Filme");
        filmePessoaFrame.add(new ChartPanel(graficoFilmePessoa));
        filmePessoaFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        filmePessoaFrame.pack();
        filmePessoaFrame.setVisible(true);
        
        /*Gráfico Categorias Mais Pops*/
        DefaultCategoryDataset dsCategoria;
        dsCategoria = filme.getCategoriaPopular();
        JFreeChart graficoCategoria = ChartFactory.createBarChart("Categorias Mais Populares", "Categoria",
                "", dsCategoria, PlotOrientation.VERTICAL, true, true, false);
        JFrame categoriaFrame = new JFrame("Categoria");
        categoriaFrame.add(new ChartPanel(graficoCategoria));
        categoriaFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        categoriaFrame.pack();
        categoriaFrame.setVisible(true);
        
        /*Vategoria mais Popular de uma Pessoa*/
        DefaultCategoryDataset dsPessoaCategoria;
        dsPessoaCategoria = filme.getPessoaCategoria("http://utfpr.edu.br/CSB30/2017/2/rodrigosouza");
        JFreeChart graficoPessoaCategoria = ChartFactory.createBarChart("Categorias Mais Populares: Rodrigo", "Categoria",
                "", dsPessoaCategoria, PlotOrientation.VERTICAL, true, true, false);
        JFrame pessoaCategoriaFrame = new JFrame("Pessoa Categoria");
        pessoaCategoriaFrame.add(new ChartPanel(graficoPessoaCategoria));
        pessoaCategoriaFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        pessoaCategoriaFrame.pack();
        pessoaCategoriaFrame.setVisible(true);
        
        /*Pessoas Mais 'Rigorosas'*/
        DefaultCategoryDataset dsRigorosos;
        dsRigorosos = pessoa.getMaisRigorosos();
        JFreeChart graficoRigorosos = ChartFactory.createBarChart("Os Mais Rigorosos", "AVG Nota",
                "", dsRigorosos, PlotOrientation.VERTICAL, true, true, false);
        JFrame rigorososFrame = new JFrame("Pessoas Mais Rigorosas");
        rigorososFrame.add(new ChartPanel(graficoRigorosos));
        rigorososFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        rigorososFrame.pack();
        rigorososFrame.setVisible(true);
        
        /*Diretores mais Pops*/
        DefaultCategoryDataset dsDiretoresPop;
        dsDiretoresPop = filme.getDiretoresPop();
        JFreeChart graficoDiretoresPop = ChartFactory.createBarChart("Diretores Pop", "Diretor",
                "", dsDiretoresPop, PlotOrientation.VERTICAL, true, true, false);
        JFrame diretoresPopFrame = new JFrame("Diretores Pop");
        diretoresPopFrame.add(new ChartPanel(graficoDiretoresPop));
        diretoresPopFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        diretoresPopFrame.pack();
        diretoresPopFrame.setVisible(true);
        
        /* View ConheceNormalizada
        *
        *
        CREATE VIEW conhecenormalizada AS (
            SELECT DISTINCT * FROM (
                SELECT matricula_1, matricula_2 FROM contato
                UNION
                SELECT matricula_2, matricula_1 FROM contato
            ) AS uniao ORDER BY matricula_1
        );
        *
        *
	Fim da View*/
        
    }
}
