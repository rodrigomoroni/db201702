/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import ConnectionFactory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.DefaultCategoryDataset;
import pojo.Pessoa;

/**
 *
 * @author rodrigo
 */
public class PessoaDao {
    Connection connection = new ConnectionFactory().getConnection();
    
    public int GetQtdConhecidos(String id){
        int count = 0;
        try {
            PreparedStatement queryConhecidos = connection.prepareStatement("SELECT count(*)"
                    + " FROM conhecenormalizada c1, conhecenormalizada c2"
                    + " WHERE c1.matricula_1 = '" + id + "' AND c1.matricula_2 = c2.matricula_1;");
            ResultSet rs = queryConhecidos.executeQuery();
            rs.next();
            count = rs.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
    
    public List<Pessoa> GetAllFilmesConhecidos(){
        List<Pessoa> pessoas = new ArrayList<>();
        try{
            PreparedStatement queryFilmesConhecido = connection.prepareStatement("SELECT c.matricula_1, c.matricula_2, count(*)"
                    + " FROM gostafilme gf1, gostafilme gf2, conhecenormalizada c"
                    + " WHERE gf1.matricula = c.matricula_1 AND gf2.matricula = c.matricula_2"
                    + " AND gf1.identificador = gf2.identificador GROUP BY c.matricula_1, c.matricula_2"
                    + " ORDER BY count(*) DESC;");
            ResultSet rs = queryFilmesConhecido.executeQuery();
            while (rs.next()){
                Pessoa pessoa = new Pessoa(rs.getString(1));
                pessoa.setConhecido(new Pessoa(rs.getString(2)));
                pessoas.add(pessoa);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return pessoas;
    }
    public DefaultCategoryDataset getMaisRigorosos(){
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try{
            PreparedStatement queryCategoria = connection.prepareStatement("SELECT p.nome, avg(gf.nota) "
                    + "FROM pessoa p, gostafilme gf "
                    + "WHERE p.matricula = gf.matricula "
                    + "GROUP BY p.nome ORDER BY avg(gf.nota) LIMIT 10;");
            ResultSet rs = queryCategoria.executeQuery();
            while(rs.next()){
                ds.addValue(rs.getInt(2), "Pessoa", rs.getString(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return ds;
    }
}
