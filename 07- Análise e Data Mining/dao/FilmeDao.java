/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import ConnectionFactory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.DefaultCategoryDataset;
import pojo.Filme;

/**
 *
 * @author rodrigo
 */
public class FilmeDao {

    Connection connection = new ConnectionFactory().getConnection();

    public double getMedia() {
        double media = 0.0;
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT sum(nota), count(*) FROM gostafilme;");
            ResultSet rs = queryRating.executeQuery();
            rs.next();
            double soma = rs.getInt(1);
            double count = rs.getInt(2);
            media = soma / count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return media;
    }

    public double getDesvioPadrao() {
        double desvioPadrao = 0.0;
        double media = this.getMedia();
        int count = 0;
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT nota FROM gostafilme;");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                count++;
                desvioPadrao += Math.pow(rs.getInt("nota") - media, 2);
            }
            desvioPadrao = desvioPadrao / count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return desvioPadrao;
    }

    public List<Filme> getMaioresRatingMedio() {
        List<Filme> filmes = new ArrayList<>();
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT filme.identificador, count(*) "
                    + "FROM filme, gostafilme WHERE filme.identificador = gostafilme.identificador GROUP BY filme.identificador ORDER BY avg(gostafilme.nota) DESC");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                if (rs.getInt(2) > 1) {
                    PreparedStatement queryFilme = connection.prepareStatement("SELECT nome FROM filme "
                            + "WHERE identificador = '" + rs.getString(1) + "'");
                    ResultSet rsFilme = queryFilme.executeQuery();
                    rsFilme.next();
                    Filme filme = new Filme(rs.getString("identificador"), rs.getInt(2), rsFilme.getString(1));
                    filmes.add(filme);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filmes;
    }

    public List<Filme> getMaisPop() {
        List<Filme> filmes = new ArrayList<>();
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT filme.identificador FROM filme, gostafilme "
                    + "WHERE filme.identificador = gostafilme.identificador GROUP BY filme.identificador "
                    + "ORDER BY count(*) DESC LIMIT 10");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                PreparedStatement queryFilme = connection.prepareStatement("SELECT nome FROM filme "
                        + "WHERE identificador = '" + rs.getString(1) + "'");
                ResultSet rsFilme = queryFilme.executeQuery();
                rsFilme.next();
                Filme filme = new Filme(rs.getString("identificador"), 0, rsFilme.getString(1));
                filmes.add(filme);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filmes;
    }

    public DefaultCategoryDataset getPessoaFilme() {
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try {
            PreparedStatement queryPF = connection.prepareStatement("SELECT count(*) FROM pessoa, gostafilme "
                    + "WHERE pessoa.matricula = gostafilme.matricula GROUP BY pessoa.matricula ORDER BY count(*)");
            ResultSet rs = queryPF.executeQuery();
            rs.next();
            int val = rs.getInt(1);
            int qtd = 1; //inicia em 1 pra contar o inicial
            while (rs.next()){
                if (rs.getInt(1) == val){
                    qtd++;
                }
                else{
                    ds.addValue(qtd, "Quantidade", val + "");
                    qtd = 1;
                    val = rs.getInt(1);
                }
            }
            ds.addValue(qtd, "Quantidade", val + ""); //quando sair do laço ainda faltará um para add, por isso está fora do laço
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ds;
    }
    
    public DefaultCategoryDataset getFilmePessoa() {
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try {
            PreparedStatement queryPF = connection.prepareStatement("SELECT count(*) FROM pessoa, gostafilme "
                    + "WHERE pessoa.matricula = gostafilme.matricula GROUP BY gostafilme.identificador ORDER BY count(*)");
            ResultSet rs = queryPF.executeQuery();
            rs.next();
            int val = rs.getInt(1);
            int qtd = 1; //inicia em 1 pra contar o inicial
            while (rs.next()){
                if (rs.getInt(1) == val){
                    qtd++;
                }
                else{
                    ds.addValue(qtd, "Quantidade", val + "");
                    qtd = 1;
                    val = rs.getInt(1);
                }
            }
            ds.addValue(qtd, "Quantidade", val + ""); //quando sair do laço ainda faltará um para add, por isso está fora do laço
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ds;
    }
    
    public DefaultCategoryDataset getCategoriaPopular(){
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try{
            PreparedStatement queryCategoria = connection.prepareStatement("SELECT nome_categoria, "
                    + "count(*) FROM pertencecategoria GROUP BY nome_categoria "
                    + "ORDER BY count(*) DESC");
            ResultSet rs = queryCategoria.executeQuery();
            while(rs.next()){
                ds.addValue(rs.getInt(2), "Quantidade", rs.getString(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return ds;
    }
    public DefaultCategoryDataset getPessoaCategoria(String id){
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try{
            PreparedStatement queryCategoria = connection.prepareStatement("SELECT pc.nome_categoria, count(*) FROM gostafilme gf, pertencecategoria pc"
                +" WHERE gf.matricula = '" + id + "' AND gf.identificador = pc.identificador"
                +" GROUP BY pc.nome_categoria ORDER BY count(*) DESC;");
            ResultSet rs = queryCategoria.executeQuery();
            while(rs.next()){
                ds.addValue(rs.getInt(2), "Quantidade", rs.getString(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return ds;
    }
    public DefaultCategoryDataset getDiretoresPop(){
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try{
            PreparedStatement queryDiretoresPop = connection.prepareStatement("SELECT identificador_p, count(*) "
                    + "FROM produz GROUP BY identificador_p "
                    + "ORDER BY count(*) DESC LIMIT 10;");
            ResultSet rs = queryDiretoresPop.executeQuery();
            while(rs.next()){
                ds.addValue(rs.getInt(2), "Quantidade", rs.getString(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return ds;
    }
}
