/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import ConnectionFactory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.DefaultCategoryDataset;
import pojo.Banda;

/**
 *
 * @author rodrigo
 */
public class BandaDao {

    Connection connection = new ConnectionFactory().getConnection();

    public double getMedia() {
        double media = 0.0;
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT sum(nota), count(nota) FROM gostaartista;");
            ResultSet rs = queryRating.executeQuery();
            rs.next();
            double soma = rs.getInt(1);
            double count = rs.getInt(2);
            media = soma / count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return media;
    }

    public double getDesvioPadrao() {
        double desvioPadrao = 0.0;
        double media = this.getMedia();
        int count = 0;
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT nota FROM gostaartista;");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                count++;
                desvioPadrao += Math.pow(rs.getInt("nota") - media, 2);
            }
            desvioPadrao = desvioPadrao / count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return desvioPadrao;
    }

    public List<Banda> getMaioresRatingMedio() {
        List<Banda> bandas = new ArrayList<>();
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT artistas_musicas.identificador, count(gostaartista.nota) "
                    + "FROM artistas_musicas, gostaartista WHERE artistas_musicas.identificador = gostaartista.identificador "
                    + "GROUP BY artistas_musicas.identificador ORDER BY avg(gostaartista.nota) DESC");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                if (rs.getInt(2) > 1) {
                    PreparedStatement queryBanda = connection.prepareStatement("SELECT nome_artisitico FROM artistas_musicas "
                            + "WHERE identificador = '" + rs.getString(1) + "'");
                    ResultSet rsBanda = queryBanda.executeQuery();
                    rsBanda.next();
                    Banda banda = new Banda(rs.getString("identificador"), rs.getInt(2), rsBanda.getString(1));
                    bandas.add(banda);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bandas;
    }

    public List<Banda> getMaisPop() {
        List<Banda> bandas = new ArrayList<>();
        try {
            PreparedStatement queryRating = connection.prepareStatement("SELECT artistas_musicas.identificador "
                    + "FROM artistas_musicas, gostaartista WHERE artistas_musicas.identificador = gostaartista.identificador "
                    + "GROUP BY artistas_musicas.identificador ORDER BY count(gostaartista.nota) DESC LIMIT 10");
            ResultSet rs = queryRating.executeQuery();
            while (rs.next()) {
                PreparedStatement queryBanda = connection.prepareStatement("SELECT nome_artisitico FROM artistas_musicas "
                        + "WHERE identificador = '" + rs.getString(1) + "'");
                ResultSet rsBanda = queryBanda.executeQuery();
                rsBanda.next();
                Banda banda = new Banda(rs.getString("identificador"), 0, rsBanda.getString(1));
                bandas.add(banda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bandas;
    }
    public DefaultCategoryDataset getPessoaBanda() {
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        try {
            PreparedStatement queryPB = connection.prepareStatement("SELECT count(gostaartista.nota) FROM pessoa, gostaartista "
                    + "WHERE pessoa.matricula = gostaartista.matricula GROUP BY pessoa.matricula ORDER BY count(gostaartista.nota)");
            ResultSet rs = queryPB.executeQuery();
            rs.next();
            int val = rs.getInt(1);
            int qtd = 1; //inicia em 1 pra contar o inicial
            while (rs.next()){
                if (rs.getInt(1) == val){
                    qtd++;
                }
                else{
                    ds.addValue(qtd, "Quantidade", val + "");
                    qtd = 1;
                    val = rs.getInt(1);
                }
            }
            ds.addValue(qtd, "Quantidade", val + ""); //quando sair do laço ainda faltará um para add, por isso está fora do laço
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ds;
    }
}
