
# Estrutura b�sica do trabalho de CSB30 - Introdu��o A Bancos De Dados

Modifique este aquivo README.md com as informa��es adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usu�rio GitLab para cada integrante do grupo.

- Buranello Ambr�sio, 1206516, Buranello Ambr�sio @buranello
- Rodrigo Gi�como Moroni de Souza, 1795481, Rodrigo Gi�como Moroni de Souza @rodrigomoroni


## Descri��o da aplica��o a ser desenvolvida 

Descreva aqui uma vis�o geral da aplica��o que ser� desenvolvida pelo grupo durante o semestre. **Este texto dever� ser escrito quando requisitado pelo professor.** O conte�do vai evoluir � medida em que o grupo avan�a com a implementa��o.
