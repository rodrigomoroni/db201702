/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ParsingXml;

import ConnectionFactory.ConnectionFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author rodrigo
 */
public class parsing {

    public static void main(String argv[]) {

        try {
            //dessa forma ficou procedural, mas é apenas para exemplificar

            Connection connection = new ConnectionFactory().getConnection(); //obtenho conexão com db
            //caso seja necessário alterar o db que será conectado, essa alteração deve ser feita no ConnectionFactory

            //URL's dos XML's, já executo a conexão...
            URLConnection urlPessoas = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml").openConnection();
            URLConnection urlArtistas = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml").openConnection();
            URLConnection urlFilmes = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml").openConnection();
            URLConnection urlConhecidos = new URL("http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml").openConnection();

            //padrão
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            //insere Pessoas no banco
            InputStream stream = urlPessoas.getInputStream(); //crio o inputstream da url da pessoa
            org.w3c.dom.Document doc = dBuilder.parse(stream); //faço a conversão
            NodeList nList = doc.getElementsByTagName("Person"); //pego os elementos com a tag
            doc.getDocumentElement().normalize(); //padrão

            //passo por cada elemento
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode; //transformo item em um elemento
                //PreparedStatement -> padrão para executar sql no JDBC

                PreparedStatement query = connection.prepareStatement("SELECT * FROM pessoa WHERE matricula = "
                        + "'" + eElement.getAttribute("uri") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO pessoa VALUES('"
                            + eElement.getAttribute("uri")
                            + "','" + eElement.getAttribute("name")
                            + "','" + eElement.getAttribute("hometown") + "');");

                    stmt.execute(); //o SQL especificado acima será executado aqui
                    stmt.close(); //e fecha aqui (padrão tbm)
                }
            }

            /**
             * *
             *
             *
             * IDEM para os demais XML, apenas ajustando os valores.
             *
             *
             ***
             */
            //insere em GostaArtista
            stream = urlArtistas.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("LikesMusic");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                //insere artistas
                PreparedStatement query = connection.prepareStatement("SELECT * FROM artistas_musicas WHERE identificador = "
                        + "'" + eElement.getAttribute("bandUri") + "'");
                ResultSet rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO artistas_musicas VALUES('"
                            + eElement.getAttribute("bandUri") + "');");
                    stmt.execute();
                    stmt.close();
                }

                //insere gosta artista
                query = connection.prepareStatement("SELECT * FROM gostaartista WHERE"
                        + " identificador='" + eElement.getAttribute("bandUri")
                        + "' AND matricula='" + eElement.getAttribute("person") + "';");
                rs = query.executeQuery();
                if (!rs.next()) {
                    PreparedStatement stmt = connection.prepareStatement("INSERT INTO gostaartista VALUES('"
                            + eElement.getAttribute("bandUri")
                            + "','" + eElement.getAttribute("person")
                            + "'," + eElement.getAttribute("rating") + ");");
                    stmt.execute();
                    stmt.close();
                }

            }

            //insere em GostaFilme
            stream = urlFilmes.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("LikesMovie");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                
                //insere filme
                PreparedStatement query = connection.prepareStatement("SELECT * FROM filme WHERE "
                        + "identificador='"+eElement.getAttribute("movieUri")+"';");
                ResultSet rs = query.executeQuery();
                if (!rs.next()){
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO filme VALUES('"
                        + eElement.getAttribute("movieUri") + "');");
                stmt.execute();
                stmt.close();
                }
                
                query = connection.prepareStatement("SELECT * FROM gostafilme WHERE"
                        + " identificador='"+eElement.getAttribute("movieUri")+"' AND "
                        + "matricula='"+ eElement.getAttribute("person")+"';");
                rs = query.executeQuery();
                if (!rs.next()){
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO gostafilme VALUES('"
                        + eElement.getAttribute("movieUri")
                        + "','" + eElement.getAttribute("person")
                        + "'," + eElement.getAttribute("rating") + ");");
                stmt.execute();
                stmt.close();
                }
            }
            
            //insere contatos
            stream = urlConhecidos.getInputStream();
            doc = dBuilder.parse(stream);
            nList = doc.getElementsByTagName("Knows");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                PreparedStatement query = connection.prepareStatement("SELECT * FROM contato WHERE"
                        + " matricula_1 = '"+ eElement.getAttribute("person")+"'"
                        + " AND matricula_2='" + eElement.getAttribute("colleague") + "';");
                ResultSet rs = query.executeQuery();
                if (!rs.next()){
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO contato VALUES('"
                        + eElement.getAttribute("person")
                        + "','" + eElement.getAttribute("colleague")
                        + "');");
                stmt.execute();
                stmt.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
