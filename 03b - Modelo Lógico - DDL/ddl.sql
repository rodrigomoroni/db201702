CREATE TABLE Turma(
    codigo VARCHAR(10) NOT NULL,
    num_alunos INTEGER NOT NULL,
    PRIMARY KEY(codigo)
);
CREATE TABLE Pessoa(
    matricula VARCHAR(100) NOT NULL,
    nome VARCHAR(40),
    cidade VARCHAR(30),
	senha VARCHAR(50),
    PRIMARY KEY(matricula)
);
CREATE TABLE TurmaPessoa(
    codigo VARCHAR(10) NOT NULL,
    matricula VARCHAR(100) NOT NULL,
    PRIMARY KEY(codigo, matricula),
    FOREIGN KEY(codigo)
        REFERENCES Turma(codigo)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(matricula)
        REFERENCES Pessoa(matricula)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);
CREATE TABLE Contato(
	matricula_1 VARCHAR(100) NOT NULL,
	matricula_2 VARCHAR(100) NOT NULL,
	razao_bloq VARCHAR(100),
	grau_amizade VARCHAR(20),
	bloqueado BOOLEAN,
	PRIMARY KEY(matricula_1, matricula_2),
	FOREIGN KEY(matricula_1)
		REFERENCES Pessoa(matricula)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
	FOREIGN KEY(matricula_2)
		REFERENCES Pessoa(matricula)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);
CREATE TABLE Filme(
	identificador VARCHAR(100) NOT NULL,
	data_lancamento TIMESTAMP,
	nome VARCHAR(50),
	PRIMARY KEY(identificador)
);
CREATE TABLE GostaFilme(
	identificador VARCHAR(100) NOT NULL,
	matricula VARCHAR(100) NOT NULL,
	nota INTEGER NOT NULL,
	PRIMARY KEY(identificador, matricula),
    FOREIGN KEY(identificador)
        REFERENCES Filme(identificador)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    FOREIGN KEY(matricula)
        REFERENCES Pessoa(matricula)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

CREATE TABLE Categoria(
  	nome VARCHAR(50) NOT NULL,
  	nome_sub VARCHAR(50),
  	PRIMARY KEY(nome),
	FOREIGN KEY(nome_sub)
		REFERENCES Categoria(nome)
		ON DELETE NO ACTION
        ON UPDATE NO ACTION
);
CREATE TABLE PertenceCategoria(
  identificador VARCHAR(100) NOT NULL,
  nome_categoria VARCHAR(50) NOT NULL,
  PRIMARY KEY(identificador,nome_categoria),
  FOREIGN KEY(identificador)
      REFERENCES Filme(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(nome_categoria)
      REFERENCES Categoria(nome)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);
CREATE TABLE Produtor(
  identificador VARCHAR(100) NOT NULL,
  telefone INTEGER,
  endereco VARCHAR(40),
  PRIMARY KEY(identificador)
);
CREATE TABLE Produz(
  identificador_f VARCHAR(100) NOT NULL,
  identificador_p VARCHAR(100) NOT NULL,
  funcao VARCHAR(10) NOT NULL,
  PRIMARY KEY(identificador_f,identificador_p),
  FOREIGN KEY(identificador_f)
      REFERENCES Filme(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(identificador_p)
      REFERENCES Produtor(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);
CREATE TABLE Artistas_musicas(
  identificador VARCHAR(100) NOT NULL,
  nome_artisitico VARCHAR(50),
  genero VARCHAR(50),
  pais VARCHAR(50),
  cidade VARCHAR(50),
  PRIMARY KEY(identificador)
);
CREATE TABLE GostaArtista(
  identificador VARCHAR(100) NOT NULL,
  matricula VARCHAR(100) NOT NULL,
  nota INTEGER NOT NULL,
  PRIMARY KEY(identificador,matricula),
  FOREIGN KEY(identificador)
      REFERENCES Artistas_musicas(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(matricula)
      REFERENCES Pessoa(matricula)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);
CREATE TABLE Musico(
  identificador VARCHAR(100) NOT NULL,
  nome VARCHAR(40) NOT NULL,
  estilo VARCHAR(10) NOT NULL,
  dt_nsc TIMESTAMP NOT NULL,
  PRIMARY KEY(identificador)
);
CREATE TABLE PossuiMusico(
  identificador_a VARCHAR(100) NOT NULL,
  identificador_m VARCHAR(100) NOT NULL,
  PRIMARY KEY(identificador_a,identificador_m),
  FOREIGN KEY(identificador_a)
      REFERENCES Artistas_musicas(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(identificador_m)
      REFERENCES Musico(identificador)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

/**INSERT's**/
INSERT INTO artistas_musicas VALUES ('0', 'RPM', 'Rock', 'Brasil');
INSERT INTO artistas_musicas VALUES ('1', 'Tom Jobim', 'Bossa-Nova', 'Brasil');
INSERT INTO artistas_musicas VALUES ('2', 'Legião Urbana', 'Rock', 'Brasil');
INSERT INTO categoria(nome) VALUES('Ficção Científica');
INSERT INTO Categoria(nome) VALUES('Animação');
INSERT INTO Categoria(nome, nome_sub) VALUES('Stop Motion', 'Animação');
INSERT INTO pessoa VALUES('1795481', 'Rodrigo Moroni', 'Porto Velho', '123456');
INSERT INTO pessoa VALUES('1794086', 'Viviane Bonfim', 'Ouro Preto do Oeste', 'password');
INSERT INTO pessoa VALUES('1', 'Fulano', 'Curitiba', 'senha');
INSERT INTO pessoa VALUES('0', 'Derpina', 'Cuiába', 'senha2');
INSERT INTO pessoa VALUES('3', 'Derp', 'Florianopólis', 'senha3');
INSERT INTO pessoa VALUES('2', 'Gsus', 'Belém', 'fodase');
INSERT INTO contato VALUES('1795481', '1794086');
INSERT INTO contato VALUES('1795481', '0');
INSERT INTO contato VALUES('1795481', '1');
INSERT INTO contato VALUES('1795481', '2');
INSERT INTO contato VALUES('1795481', '3');
INSERT INTO contato VALUES('1', '2');
INSERT INTO filme VALUES('0', '1977-01-01', 'Star Wars IV');
INSERT INTO filme VALUES('1', '1987-01-01', 'Full Metal Jacket');
INSERT INTO filme VALUES('2', '1994-01-01', 'Pulp Fiction');
INSERT INTO filme VALUES('3', '1998-01-01', 'Central do Brasil');
INSERT INTO filme VALUES('4', '2002-01-01', 'Cidade de Deus');
INSERT INTO gostafilme VALUES('0', '1795481', '5');
INSERT INTO gostafilme VALUES('1', '1795481', '5');
INSERT INTO gostafilme VALUES('2', '1795481', '5');
INSERT INTO gostafilme VALUES('3', '1795481', '5');
INSERT INTO gostafilme VALUES('4', '1795481', '5');
INSERT INTO gostafilme VALUES('0', '1794086', '5');
INSERT INTO gostafilme VALUES('1', '1794086', '5');
INSERT INTO gostaartista VALUES('0', '1795481', '5');
INSERT INTO gostaartista VALUES('1', '1795481', '5');
INSERT INTO gostaartista VALUES('2', '1795481', '5');
INSERT INTO gostaartista VALUES('0', '1794086', '4');
INSERT INTO gostaartista VALUES('1', '1794086', '4');
INSERT INTO gostaartista VALUES('2', '1794086', '5');
INSERT INTO musico VALUES('0', 'Dado Villa-Lobos', 'Rock', '1789-01-01');
INSERT INTO musico VALUES('1', 'Renato Russo', 'Rock', '1789-01-01');
INSERT INTO musico VALUES('2', 'Marcelo Bonfá', 'Rock', '1789-01-01');
INSERT INTO musico VALUES('3', 'Renato Rocha', 'Rock', '1789-01-01');
INSERT INTO musico VALUES('4', 'Antônio Carlos Jobim', 'Bossa-Nova', '1789-01-01');
INSERT INTO musico VALUES('5', 'Paulo Ricardo', 'Rock', '1789-01-01');
INSERT INTO pertencecategoria VALUES('0', 'Ficção Científica');
INSERT INTO pertencecategoria VALUES('1', 'Ficção Científica');
INSERT INTO pertencecategoria VALUES('2', 'Ficção Científica');
INSERT INTO pertencecategoria VALUES('3', 'Ficção Científica');
INSERT INTO pertencecategoria VALUES('4', 'Ficção Científica');
INSERT INTO pertencecategoria VALUES('0', 'Animação');
INSERT INTO possuimusico VALUES('2', '0');
INSERT INTO possuimusico VALUES('2', '1');
INSERT INTO possuimusico VALUES('2', '2');
INSERT INTO possuimusico VALUES('2', '3');
INSERT INTO possuimusico VALUES('1', '4');
INSERT INTO possuimusico VALUES('0', '5');
INSERT INTO produtor VALUES('0', 3222-8222, 'Rua das Bruxas');
INSERT INTO produtor VALUES('1', 3222-8222, 'Rua das Flores');
INSERT INTO produtor VALUES('2', 3222-8222, 'Rua Sem Fim');
INSERT INTO produtor VALUES('3', 3222-8222, 'Rua Sete');
INSERT INTO produtor VALUES('4', 3222-8222, 'Rua 13');
INSERT INTO produtor VALUES('5', 3222-8222, 'Rua Silas Schockness');
INSERT INTO produz VALUES('0', '0', 'Ator');
INSERT INTO produz VALUES('0', '1', 'Atriz');
INSERT INTO produz VALUES('1', '0', 'Diretor');
INSERT INTO produz VALUES('0', '2', 'Diretor');
INSERT INTO produz VALUES('1', '2', 'Atriz');
INSERT INTO produz VALUES('2', '3', 'Ator');
INSERT INTO turma VALUES('CSG20s73', 26);
INSERT INTO turma VALUES('CSG20s71', 16);
INSERT INTO turma VALUES('CSG20s72', 06);
INSERT INTO turma VALUES('CSG20s11', 46);
INSERT INTO turma VALUES('CSG20s21', 36);
INSERT INTO turma VALUES('CSG20s01', 76);
INSERT INTO turmapessoa VALUES('CSG20s73', '1795481');
INSERT INTO turmapessoa VALUES('CSG20s71', '1794086');
INSERT INTO turmapessoa VALUES('CSG20s72', '0');
INSERT INTO turmapessoa VALUES('CSG20s11', '1');
INSERT INTO turmapessoa VALUES('CSG20s21', '2');
INSERT INTO turmapessoa VALUES('CSG20s01', '3');
