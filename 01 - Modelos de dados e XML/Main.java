
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public Main() {
    }

    public static void main(String[] args)
            throws IOException {

        Scanner keyboard = new Scanner(System.in);
        String caminhoXML;
        String caminhoCSV;

        // /home/lucas/Desktop/Universidade/BD/marvel_simplificado.xml
        System.out.println("Digite o caminho do arquivo XML: ");
        caminhoXML = keyboard.nextLine();
        LeXML arquivo = new LeXML(caminhoXML);

        // /home/lucas/Desktop/Universidade/BD/
        System.out.println("Digite o caminho onde deseja salvar o CSV: ");
        caminhoCSV = keyboard.nextLine();

        FileWriter herois = CSVUtils.criaCSV(caminhoCSV + "/dadosMarvel/", "herois");
        arquivo.imprimeAllCSV(herois);

        herois.flush();
        herois.close();

        FileWriter herois_good = CSVUtils.criaCSV(caminhoCSV + "/dadosMarvel/", "herois_good");
        arquivo.imprimeGood(herois_good);

        herois_good.flush();
        herois_good.close();

        FileWriter herois_bad = CSVUtils.criaCSV(caminhoCSV + "/dadosMarvel/", "herois_bad");
        arquivo.imprimeBad(herois_bad);

        herois_bad.flush();
        herois_bad.close();

        arquivo.calculaProporcao();
        arquivo.calculaMediaPeso();
        arquivo.imc("hulk");
    }
}

