
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LeXML {

    private Document doc;
    private NodeList nList;

    public LeXML(String caminho) {
        try {
            File fXmlFile = new File(caminho);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            nList = doc.getElementsByTagName("hero");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ImprimeAllTela() {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            System.out.println("\nCurrent Element :" + nNode.getNodeName());

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                System.out.println("ID : " + eElement.getAttribute("id"));
                System.out.println("Name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
                System.out.println("Popularity : " + eElement.getElementsByTagName("popularity").item(0).getTextContent());
                System.out.println("Alignment : " + eElement.getElementsByTagName("alignment").item(0).getTextContent());
                System.out.println("Gender : " + eElement.getElementsByTagName("gender").item(0).getTextContent());
                System.out.println("Height : " + eElement.getElementsByTagName("height_m").item(0).getTextContent());
                System.out.println("Weight : " + eElement.getElementsByTagName("weight_kg").item(0).getTextContent());
                System.out.println("Hometown : " + eElement.getElementsByTagName("hometown").item(0).getTextContent());
                System.out.println("Intelligence : " + eElement.getElementsByTagName("intelligence").item(0).getTextContent());
                System.out.println("Strength : " + eElement.getElementsByTagName("strength").item(0).getTextContent());
                System.out.println("Speed : " + eElement.getElementsByTagName("speed").item(0).getTextContent());
                System.out.println("Durability : " + eElement.getElementsByTagName("durability").item(0).getTextContent());
                System.out.println("Energy Projection : " + eElement.getElementsByTagName("energy_Projection").item(0).getTextContent());
                System.out.println("Fighting Skilss : " + eElement.getElementsByTagName("fighting_Skills").item(0).getTextContent());
            }
        }
    }

    public void imprimeAllCSV(FileWriter writer) throws IOException {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                CSVUtils.writeLine(writer, Arrays.asList(new String[]{eElement.getAttribute("id"), eElement
                    .getElementsByTagName("name").item(0).getTextContent(), eElement
                    .getElementsByTagName("popularity").item(0).getTextContent(), eElement
                    .getElementsByTagName("alignment").item(0).getTextContent(), eElement
                    .getElementsByTagName("gender").item(0).getTextContent(), eElement
                    .getElementsByTagName("height_m").item(0).getTextContent(), eElement
                    .getElementsByTagName("weight_kg").item(0).getTextContent(), eElement
                    .getElementsByTagName("hometown").item(0).getTextContent(), eElement
                    .getElementsByTagName("intelligence").item(0).getTextContent(), eElement
                    .getElementsByTagName("strength").item(0).getTextContent(), eElement
                    .getElementsByTagName("speed").item(0).getTextContent(), eElement
                    .getElementsByTagName("durability").item(0).getTextContent(), eElement
                    .getElementsByTagName("energy_Projection").item(0).getTextContent(), eElement
                    .getElementsByTagName("fighting_Skills").item(0).getTextContent()}));
            }
        }
    }

    public void imprimeGood(FileWriter writer) throws IOException {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                if (eElement.getElementsByTagName("alignment").item(0).getTextContent().toLowerCase().equals("good")) {
                    CSVUtils.writeLine(writer, Arrays.asList(new String[]{eElement.getAttribute("id"), eElement
                        .getElementsByTagName("name").item(0).getTextContent(), eElement
                        .getElementsByTagName("popularity").item(0).getTextContent(), eElement
                        .getElementsByTagName("alignment").item(0).getTextContent(), eElement
                        .getElementsByTagName("gender").item(0).getTextContent(), eElement
                        .getElementsByTagName("height_m").item(0).getTextContent(), eElement
                        .getElementsByTagName("weight_kg").item(0).getTextContent(), eElement
                        .getElementsByTagName("hometown").item(0).getTextContent(), eElement
                        .getElementsByTagName("intelligence").item(0).getTextContent(), eElement
                        .getElementsByTagName("strength").item(0).getTextContent(), eElement
                        .getElementsByTagName("speed").item(0).getTextContent(), eElement
                        .getElementsByTagName("durability").item(0).getTextContent(), eElement
                        .getElementsByTagName("energy_Projection").item(0).getTextContent(), eElement
                        .getElementsByTagName("fighting_Skills").item(0).getTextContent()}));
                }
            }
        }
    }

    public void imprimeBad(FileWriter writer) throws IOException {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                if (eElement.getElementsByTagName("alignment").item(0).getTextContent().toLowerCase().equals("bad")) {
                    CSVUtils.writeLine(writer, Arrays.asList(new String[]{eElement.getAttribute("id"), eElement
                        .getElementsByTagName("name").item(0).getTextContent(), eElement
                        .getElementsByTagName("popularity").item(0).getTextContent(), eElement
                        .getElementsByTagName("alignment").item(0).getTextContent(), eElement
                        .getElementsByTagName("gender").item(0).getTextContent(), eElement
                        .getElementsByTagName("height_m").item(0).getTextContent(), eElement
                        .getElementsByTagName("weight_kg").item(0).getTextContent(), eElement
                        .getElementsByTagName("hometown").item(0).getTextContent(), eElement
                        .getElementsByTagName("intelligence").item(0).getTextContent(), eElement
                        .getElementsByTagName("strength").item(0).getTextContent(), eElement
                        .getElementsByTagName("speed").item(0).getTextContent(), eElement
                        .getElementsByTagName("durability").item(0).getTextContent(), eElement
                        .getElementsByTagName("energy_Projection").item(0).getTextContent(), eElement
                        .getElementsByTagName("fighting_Skills").item(0).getTextContent()}));
                }
            }
        }
    }

    public void calculaProporcao() {
        int contGood = 0;
        int contBad = 0;
        int cont = 0;
        double prop;
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                if (eElement.getElementsByTagName("alignment").item(0).getTextContent().equalsIgnoreCase("good")) {
                    contGood++;
                    cont++;
                } else if (eElement.getElementsByTagName("alignment").item(0).getTextContent().equalsIgnoreCase("bad")) {
                    contBad++;
                    cont++;
                } else {
                    cont++;
                }

            }
        }

        prop = ((double) contGood / cont) * 100;
        System.out.println("Good: " + prop + "%");
        prop = ((double) contBad / cont) * 100;
        System.out.println("Bad: " + prop + "%");
    }

    public void calculaMediaPeso() {
        int cont = 0;
        double acumula = 0;
        double media;

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                acumula += Double.parseDouble(eElement.getElementsByTagName("weight_kg").item(0).getTextContent());
                cont++;
            }
        }

        media = acumula / cont;
        System.out.println("Media de peso: " + media + "kg");
    }

    public void imc(String nome) {
        double imc;
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == 1) {
                Element eElement = (Element) nNode;

                if (eElement.getElementsByTagName("name").item(0).getTextContent().equalsIgnoreCase(nome)) {
                            imc = Double.parseDouble(eElement.getElementsByTagName("weight_kg").item(0).getTextContent())
                                    /(Math.pow(Double.parseDouble(eElement.getElementsByTagName("height_m").item(0).getTextContent()), 2));
                
                            System.out.println("IMC " + nome + ": " + imc);
                }
            }
        }
    }
}

