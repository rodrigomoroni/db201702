/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd06;

import ConnectionFactory.ConnectionFactory;
import static com.sun.javafx.util.Utils.split;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jsoup.Jsoup;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class main {

    public static void main(String argv[]) {
        try {
            Connection connection = new ConnectionFactory().getConnection(); //obtenho conexão com db

            /**
             * ******FILMES******
             */
            PreparedStatement queryFilmes = connection.prepareStatement("SELECT identificador FROM filme"); //busco os filmes no db
            ResultSet rs = queryFilmes.executeQuery(); //salvo resultado da query

            /*salvar em xml*/
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance(); //padrão
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); //padrão
            Document doc = dBuilder.newDocument(); //crio novo doc (xml)
            Element rootElement = doc.createElement("Filmes"); //crio um novo elemento (Tag)
            doc.appendChild(rootElement); //adiciono a tag no doc, logo ela é a primeira (root)
            Element filmeElement; //declaro o elemento de filme, será tag filha da anterior (child)

            while (rs.next()) //enquanto houver resultados
            {
                org.jsoup.nodes.Document document = Jsoup.connect(rs.getString("identificador")).get(); //'abre' o site

                /*nome do filme*/
                org.jsoup.select.Elements nListNome = document.getElementsByTag("title"); //uso a tag title para obter ano e titulo
                document.normalise(); //padrão
                String[] nome = split(nListNome.get(0).ownText(), " ("); //separa ex: Pulp Fiction (1994) > Pulp Fiction
                String[] ano = split(nome[1], ")"); //1994

                /*diretor do filme*/
                org.jsoup.select.Elements nListDiretor = document.getAllElements(); //pego todas tags
                document.normalise(); //padrão
                String diretor = null, diretorUrl = "http://www.imdb.com"; //inicializa
                for (int i = 0; i < nListDiretor.size(); i++) {
                    //verifico onde essa informações 'batem'
                    if ((nListDiretor.get(i).ownText().equals("Director:") || nListDiretor.get(i).ownText().equals("Directors:"))
                            && nListDiretor.get(i).hasClass("inline")) {
                        diretor = nListDiretor.get(i + 3).ownText(); //pego terceira tag após o <h4> Director: </h4>
                        diretorUrl += nListDiretor.get(i + 2).attr("href"); //pego a url do diretor (usada como identificador depois)
                        break;
                    }
                }

                /*genero do filme*/
                //só há esse lugar que dá 'match'
                org.jsoup.select.Elements nListGenero = document.getElementsByAttributeValueMatching("itemprop", "genre");
                document.normalise(); //padrão
                String genero = nListGenero.get(0).ownText(); //pego apenas o primeiro, resto não importa

                //obter outro dado de outra fonte
                /*README README README README README README README README*/
                /*rottentomatoes usa o titulo dos filmes em inglês na url o que dificulta nosso trabalho, no caso de filmes sem tradução
                como Pulp Fiction funciona, mas para filmes traduzidos ou filmes brasileiros dá ruim por isso deixei comentado, a ideia
				é pegar nota de critica do filme no rotten*/
                //org.jsoup.nodes.Document docOutraFonteFilme = Jsoup.connect("https://www.rottentomatoes.com/m/" + nome[0].replace(" ", "_")).get(); //abre o link
                //org.jsoup.select.Elements nListOutraFonteFilme = docOutraFonteFilme.getElementsByClass("meter-value superPageFontColor"); //vou conseguir tempo de atividade aqui
                String nota = null;
                /*if (!nListOutraFonteFilme.isEmpty())
                {
                    nota = nListOutraFonteFilme.get(0).children().get(0).ownText();
                }*/
                /****************************************************************************/
                
                //salva no xml as informações obtidas
                filmeElement = doc.createElement("filme");
                rootElement.appendChild(filmeElement);
                Attr tituloAttr = doc.createAttribute("titulo");
                tituloAttr.setValue(nome[0]);
                filmeElement.setAttributeNode(tituloAttr);
                Attr anoAttr = doc.createAttribute("ano");
                anoAttr.setValue(ano[0]);
                filmeElement.setAttributeNode(anoAttr);
                Attr diretorAttr = doc.createAttribute("diretor");
                diretorAttr.setValue(diretor);
                filmeElement.setAttributeNode(diretorAttr);
                Attr generoAttr = doc.createAttribute("genero");
                generoAttr.setValue(genero);
                filmeElement.setAttributeNode(generoAttr);
                Attr uri = doc.createAttribute("uri");
                uri.setValue(rs.getString("identificador"));
                filmeElement.setAttributeNode(uri);
                //extra
                Attr notaAttr = doc.createAttribute("nota");
                notaAttr.setValue(nota);
                filmeElement.setAttributeNode(notaAttr);
                
                //atualiza tabela de filme
                PreparedStatement stmt = connection.prepareStatement("UPDATE filme SET data_lancamento='" + ano[0] + "-01-01" //alguns filmes não tem mes/ano unico
                        + "', nome='" + nome[0] + "' WHERE identificador='" + rs.getString("identificador") + "';");
                stmt.execute();
                
                //atualiza tabela de categoria
                PreparedStatement queryCategoria = connection.prepareStatement("SELECT * FROM categoria WHERE nome='" + genero + "';");
                ResultSet rsCategoria = queryCategoria.executeQuery();
                if (!rsCategoria.next())
                {
                    PreparedStatement insereCategoria = connection.prepareStatement("INSERT INTO categoria VALUES ('" + genero + "');");
                    insereCategoria.execute();
                }
                //verifica se categoria já tem relação com o filme
                PreparedStatement pertenceCategoria = connection.prepareStatement("SELECT * FROM pertencecategoria WHERE"
                        + " identificador='" + rs.getString("identificador") + "' AND nome_categoria='" + genero + "';");
                ResultSet rsPertence = pertenceCategoria.executeQuery();
                if(!rsPertence.next())
                {
                    PreparedStatement inserePertence = connection.prepareStatement("INSERT INTO pertencecategoria VALUES ("
                            + "'" + rs.getString("identificador") + "', '" + genero + "');");
                    inserePertence.execute();
                }
                //atualiza tabela do diretor(produtor)
                PreparedStatement queryDiretor = connection.prepareStatement("SELECT * FROM produtor WHERE identificador ='"+ diretorUrl + "';");
                ResultSet rsDiretor = queryDiretor.executeQuery();
                if (!rsDiretor.next())
                {
                    PreparedStatement insereDiretor = connection.prepareStatement("INSERT INTO produtor VALUES ('" + diretorUrl + "');");
                    insereDiretor.execute();
                }
                //verifica se não existe relação com filme
                PreparedStatement queryProduz = connection.prepareStatement("SELECT * FROM produz WHERE identificador_f='" + rs.getString("identificador") + "'"
                    + " AND identificador_p='" + diretorUrl + "';");
                ResultSet rsProduz = queryProduz.executeQuery();
                if (!rsProduz.next())
                {
                    PreparedStatement insereProduz = connection.prepareStatement("INSERT INTO produz VALUES ('" + rs.getString("identificador") + "'"
                            + ", '" + diretorUrl + "', 'diretor');");
                    insereProduz.execute();
                }
                //print para testar se as informações estão chegando
                System.out.println("Filme:" + nome[0] + " / Ano:" + ano[0] + "-01-01" + " / Diretor:" + diretor
                        + "/ Genero:" + genero);
            }

            //Trecho que salva o doc(XML) em um arquivo.xml
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance(); //padrão
            Transformer transformer = transformerFactory.newTransformer(); //padrão
            transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //padrão
            DOMSource source = new DOMSource(doc); //padrão
            StreamResult console = new StreamResult(System.out); //manda pro console
            StreamResult file = new StreamResult(new File("/home/rodrigo/Documentos/Rodrigo/4º Período/movie.xml")); //manda pro arquivo
            transformer.transform(source, console); //resultado no console
            transformer.transform(source, file); //resultado no arquivo
            
            /****************************************************************************/
            /**
             * ARTISTAS MUSICAIS*
             */
            PreparedStatement queryArtistas = connection.prepareStatement("SELECT identificador FROM artistas_musicas"); //seleciono artistas
            rs = queryArtistas.executeQuery(); //executo a query

            /*salvar em xml*/
            //DocumentBuilderFactory dbFactoryBand = DocumentBuilderFactory.newInstance(); //padrão
            //DocumentBuilder dBuilderBand = dbFactoryBand.newDocumentBuilder(); //padrão
            Document docBand = dBuilder.newDocument(); //crio novo doc (xml)
            Element rootBand = docBand.createElement("Bandas"); //crio um novo elemento (Tag)
            docBand.appendChild(rootBand); //adiciono a tag no doc, logo ela é a primeira (root)
            Element bandElement; //declaro o elemento de filme, será tag filha da anterior (child)

            while (rs.next()) {
                org.jsoup.nodes.Document document = Jsoup.connect(rs.getString("identificador")).get(); //abre o link

                //nome da banda
                org.jsoup.select.Elements nListNome = document.getElementsByTag("title"); //tag title
                document.normalise();  //padrão
                String[] nome = split(nListNome.get(0).ownText(), " -"); //faz o split pra não pegar 'wikipedia'

                //cidade e pais de origem / genero
                org.jsoup.select.Elements nListCep = document.getAllElements();
                document.normalise();  //padrão
                String cidade = null, pais = null, genero = null;
                for (int i = 0; i < nListCep.size(); i++) {
                    //cidade e pais
                    if (nListCep.get(i).ownText().equals("Origin")) {
                        if (nListCep.get(i + 1).ownText().equals(","))
                        {
                            cidade = nListCep.get(i + 2).ownText();
                            pais = nListCep.get(i + 3).ownText();
                        }
                        else if (nListCep.get(i + 1).ownText().length() > 10 && !nListCep.get(i + 1).ownText().startsWith(","))
                        {
                            String[] aux = split(nListCep.get(i + 1).ownText(), ",");
                            if (aux.length > 0)
                            {
                            cidade = aux[0];
                            pais = aux[1];
                            }
                        }
                        else if (nListCep.get(i + 1).ownText().startsWith(","))
                        {
                            cidade = nListCep.get(i + 2).ownText();
                            pais = nListCep.get(i + 1).ownText().replace(", ", "");
                        }
                        else
                        {
                            cidade = nListCep.get(i + 2).ownText();
                        }
                    }
                    //genero
                    if (nListCep.get(i).ownText().equals("Genres") && nListCep.get(i).hasAttr("scope")){
                        genero = nListCep.get(i + 1).ownText();
                        int count = 1;
                        while (genero.startsWith(",") || genero.isEmpty())
                        {
                            genero = nListCep.get(i + count).ownText();
                            count++;
                        }
                    }
                }
                
                //outro dado de outra fonte
                //https://www.last.fm/music/
                /*README README README README README README README README*/
                /**eu deixei comentando pois algumas bandas não existem no last.fm, daí ele gera uma exceção
                 mas para as bandas cadastradas lá funciona pegar atividade, seria algo do tipo: Led Zeppelin
                 Atividade = (1968 – 1980)**/
                String[] splitNome = split(nome[0], "(");
                String nome_banda = splitNome[0].replace(" ", "_");
                //org.jsoup.nodes.Document docOutraFonte = Jsoup.connect("https://www.last.fm/music/" + nome_banda).get(); //abre o link
                //org.jsoup.select.Elements nListOutraFonte = docOutraFonte.getElementsByClass("factbox-summary"); //vou conseguir tempo de atividade aqui             
                String atividade = null; //atividade(tempo) da banda
                /*if (!nListOutraFonte.isEmpty())
                {
                    String[] aux = split(nListOutraFonte.get(0).ownText(), " (");
                    atividade = aux[1].replace(")", "");
                }*/
                /******************************************************************/
                
                //salva no xml as informações obtidas
                bandElement = docBand.createElement("banda");
                rootBand.appendChild(bandElement);
                //atributos
                Attr tituloAttr = docBand.createAttribute("nome");
                tituloAttr.setValue(nome[0]);
                bandElement.setAttributeNode(tituloAttr);
                Attr anoAttr = docBand.createAttribute("cidade");
                anoAttr.setValue(cidade);
                bandElement.setAttributeNode(anoAttr);
                Attr diretorAttr = docBand.createAttribute("pais");
                diretorAttr.setValue(pais);
                bandElement.setAttributeNode(diretorAttr);
                Attr generoAttr = docBand.createAttribute("genero");
                generoAttr.setValue(genero);
                bandElement.setAttributeNode(generoAttr);
                Attr uri = docBand.createAttribute("uri");
                uri.setValue(rs.getString("identificador"));
                bandElement.setAttributeNode(uri);
                //atributo extra
                Attr atividadeAttr = docBand.createAttribute("atividade");
                atividadeAttr.setValue(atividade);
                bandElement.setAttributeNode(atividadeAttr);
                
                //atualiza tabela no banco
                String nomeSemAspas = nome[0].replace("\'", ""); //culpa do Guns N' Roses
                PreparedStatement stmt = connection.prepareStatement("UPDATE artistas_musicas SET nome_artisitico ='" + nomeSemAspas
                        + "', genero='" + genero + "', cidade='" + cidade + "', pais='" + pais + "' WHERE identificador = '"
                        + rs.getString("identificador") + "';");
                stmt.execute();

                //imprime pra verificar se dados estão corretos
                System.out.println("Banda:" + nome[0] + "/ Cidade:" + cidade + "/ País:" + pais
                    + "/ Genero:" + genero);
            }
            
            //Trecho que salva o doc(XML) em um arquivo.xml
            transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //padrão
            DOMSource sourceBand = new DOMSource(docBand); //padrão
            StreamResult consoleBand = new StreamResult(System.out); //manda pro console
            StreamResult fileBand = new StreamResult(new File("/home/rodrigo/Documentos/Rodrigo/4º Período/music.xml")); //manda pro arquivo
            transformer.transform(sourceBand, consoleBand); //resultado no console
            transformer.transform(sourceBand, fileBand); //resultado no arquivo

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
