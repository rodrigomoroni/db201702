package ConnectionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class ConnectionFactory 
{
    public Connection getConnection() 
    {
        try 
        {
            return DriverManager.getConnection(
                    "jdbc:postgresql://200.134.10.32/1702BLR", "1702BLR", "388360");
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Conexão com DB falhou", "Erro", ERROR_MESSAGE);
            throw new RuntimeException(e);
        }
    }
}
