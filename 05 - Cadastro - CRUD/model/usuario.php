<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include 'connection.php';
        
        //classe modelUsuario contém os métodos de interação com db referente ao usuário
        class modelUsuario {
            
            //função que cadastra um usuário no db
            public function cadastro($login, $senha, $nome, $cidade) {
                $connection = new connection(); //classe de conexão
                $con = $connection->connect(); //chamo o método de conexão
                $senha = sha1($senha); //criptografa a senha
                $query = "INSERT INTO pessoa (matricula, nome, cidade, senha) "
                        . "VALUES ('$login', '$nome', '$cidade', '$senha');"; //String contendo a Query
                return pg_query($con, $query); //executa a Query e retorna o resultado
            }
            
            public function login($login, $senha){
                $connection = new connection();
                $con = $connection->connect();
                $senha = sha1($senha);
                $query = "SELECT * FROM pessoa WHERE matricula='$login' AND senha='$senha';";
                $res = pg_query($con, $query);
                if ($retorno = pg_fetch_assoc($res)) { //se houver retorno, retorna true
                    return true;
                } else {
                    return false;
                }
            }
            
            public function getByLogin($login){
                $connection = new connection();
                $con = $connection->connect();
                $query = "SELECT * FROM pessoa WHERE matricula='$login';";
                return pg_query($con, $query);
            }
            
            public function updateUser($login, $nome, $cidade)
            {
                $connection = new connection();
                $con = $connection->connect();
                $query = "UPDATE pessoa SET nome='$nome', cidade='$cidade' WHERE matricula='$login';";
                return pg_query($con, $query);
            }
            
            public function getContatos($login)
            {
                $connection = new connection();
                $con = $connection->connect();
                //SQL-> seleciona as pessoas que estão a lista de contatos de login
                $query = "SELECT * FROM pessoa WHERE matricula IN (SELECT matricula_2 FROM contato WHERE matricula_1 = '$login');";
                return pg_query($con, $query);
            }
            
            public function removeContato($login, $contato)
            {
                $connection = new connection();
                $con = $connection->connect();
                //SQL-> seleciona as pessoas que estão a lista de contatos de login
                $query = "DELETE FROM contato WHERE matricula_1 = '$login' AND matricula_2 = '$contato';";
                return pg_query($con, $query);
            }
        }
        ?>
    </body>
</html>
