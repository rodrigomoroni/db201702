Estrutura de Diretórios:
    View: as telas em HTML (interação com usuário), interage com o Controller
    Controller: back-end (verificações, geração...), interage com o Model
    Model: QUERY's e etc, interage com o Banco de Dados (PostgreSQL)
    Demais diretórios: jquery, bootstrap e arquivos de projeto (netbeans)

Banco de Dados:
    Estrutura de Tabelas: arquivo .sql no diretório "03b - DDL"
    Dados: o arquivo .sql contém alguns INSERT's, o diretório "04 - XML" também insere dados no nosso banco (local).

Caso seja necessário executar o código, o servidor web Apache precisa ter a extensão para PostgreSQL, normalmente comentada no arquivo "php.ini".
