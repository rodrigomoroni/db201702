<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <?php
        include '../model/usuario.php';

        class Usuario {

            public function cadastro() {
                //salvo os dados passados por POST no Form
                $login = $_POST['Login'];
                $senha = $_POST['Senha'];
                $nome = $_POST['Nome'];
                $cidade = $_POST['Cidade'];

                $userModel = new modelUsuario(); //classe para cadastrar no db
                $resposta = $userModel->cadastro($login, $senha, $nome, $cidade); //método para cadastrar
                if ($resposta) { //resposta positiva
                    ?>
                    <script>
                        window.alert("Cadastrado com sucesso!");
                        window.location.href = "../view/index.php";
                    </script>
                    <?php
                } else { //erro
                    ?>
                    <script>
                        window.alert("Falha no Cadastro!");
                        window.location.href = "../view/cadastroUsuario.php";
                    </script>
                    <?php
                }
            }

            public function login() {
                $login = $_POST['Login'];
                $senha = $_POST['Password'];
                $userModel = new modelUsuario(); //classe para cadastrar no db
                $resposta = $userModel->login($login, $senha); //método para logar
                if ($resposta) { //resposta positiva
                    session_start();
                    $_SESSION['login'] = $login;
                    header("location: ../view/index.php");
                } else { //erro
                    ?>
                    <script>
                        window.alert("Falha na Autenticação!");
                        window.location.href = "../view/login.php";
                    </script>
                    <?php
                }
            }

            public function getByLogin($login) {
                $userModel = new modelUsuario();
                $resultado = $userModel->getByLogin($login);
                $user = pg_fetch_assoc($resultado);
                echo 'Login: <input class="form-control input-sm" readonly="true" name="Login" type="text" value="' . $user['matricula'] . '"><br>'
                . 'Nome: <input class="form-control input-sm" name="Nome" type="text" value="' . $user['nome'] . '"><br>'
                . 'Cidade: <input class="form-control input-sm" name="Cidade" type="text" value="' . $user['cidade'] . '"><br>';
            }

            public function editar() {
                $login = $_POST['Login'];
                $nome = $_POST['Nome'];
                $cidade = $_POST['Cidade'];
                $userModel = new modelUsuario();
                $resultado = $userModel->updateUser($login, $nome, $cidade);
                if ($resultado) { //resposta positiva
                    ?>
                    <script>
                        window.alert("Alterado com Sucesso");
                        window.location.href = "../view/index.php";
                    </script>
                    <?php
                } else { //erro
                    ?>
                    <script>
                        window.alert("Falha na Edição!");
                        window.location.href = "../view/editaUsuario.php";
                    </script>
                    <?php
                }
            }

            public function listarContatos($login) {
                ?>

                <script>
                    function confirmaRem(id) {
                        var resposta = confirm("Deseja remover esse contato?");

                        if (resposta === true) {
                            window.location.href = "../view/removeContato.php?contato=" + id;
                        }
                    }
                </script>

                <?php
                $userModel = new modelUsuario();
                $resultado = $userModel->getContatos($login);
                while ($user = pg_fetch_assoc($resultado)) {
                    //echo '' . $user['nome'] . ' - <a href="../view/removeContato.php?contato='.$user['matricula'].'">Remover</a><br>';
                    echo '<p><a href="../view/viewContato.php?contato=' . $user['matricula'] . '">' . $user['nome'] . '</a> '
                            . ' <a class="col-md-offset-6 btn btn-danger" onclick="confirmaRem(' . $user['matricula'] . ')">Remover</a></p>';
                }
            }

            public function removeContato($login, $contato) {
                $userModel = new modelUsuario();
                $resultado = $userModel->removeContato($login, $contato);
                if ($resultado) { //resposta positiva
                    ?>
                    <script>
                        window.alert("Removido com Sucesso");
                        window.location.href = "../view/index.php";
                    </script>
                    <?php
                } else { //erro
                    ?>
                    <script>
                        window.alert("Falha na Remoção!");
                        window.location.href = "../view/listaContatos.php";
                    </script>
                    <?php
                }
            }

        }

//essa coisa pega o input hidden que veio no form e verifica qual metodo
//será utilizado da classe...
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['method'])) {
            $method = $_POST['method'];
            if (method_exists('Usuario', $method)) {
                $user = new Usuario;
                $user->$method($_POST);
            } else {
                echo 'Erro, entre em contato com suporte. :(';
            }
        }
        ?>
    </body>
</html>
