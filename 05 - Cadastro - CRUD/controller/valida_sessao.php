<?php

//sempre que houver necessidade de uso da 'session', deve-se chamar a função...
session_start();
//isset retorna verdadeiro se houver algo preenchido no session especificado
//então se não houver algo na session (!isset), redireciona para o viewLogin
if (!isset($_SESSION['login']) /* and !isset($_SESSION['spassword']) */) {
    header("location: ../view/login.php");
}
?>

