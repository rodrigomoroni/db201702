<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        include '../controller/valida_sessao.php';
        include '../controller/usuario.php';
        include 'padrao.php';
        ?>
        <title></title>
    </head>
    <body>
        <div style="padding-top: 10%" class="col-md-offset-4 col-md-3 ">
            <div class="well">
                <form action="../controller/usuario.php" method="POST">
                    <?php
                    $usuario = new Usuario();
                    $usuario->getByLogin($_SESSION['login'])
                    ?>
                    <input type="hidden" name="method" value="editar">
                    <input class="btn btn-primary" type="submit" value="Salvar Alterações">
                    <a href="index.php" class="btn btn-default">Voltar</a>
                </form>
            </div>
        </div>
    </body>
</html>
