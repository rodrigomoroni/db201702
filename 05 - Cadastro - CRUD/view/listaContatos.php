<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        include '../controller/valida_sessao.php';
        include '../controller/usuario.php';
        include 'padrao.php';
        ?>
        <title>Contatos</title>
    </head>
    <body>
        <div style="padding-top: 10%" class="col-md-offset-4 col-md-3 ">
            <div class="well">
                <h4>Lista de Contatos</h4>
                <?php
                $user = new Usuario();
                $user->listarContatos($_SESSION['login']);
                ?>
                <a href="#" class="btn btn-primary">Adicionar Contato</a>
                <a href="index.php" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </body>
</html>
